#!/bin/bash -x

set -e

# set yum proxy using http_proxy build arg
if [[ "$http_proxy" != "" ]]; then
    echo "proxy  = $http_proxy" >> /etc/yum.conf
fi

# install rsyslog
curl http://rpms.adiscon.com/v8-stable/rsyslog.repo \
     -o /etc/yum.repos.d/rsyslog.repo
yum install -y rsyslog rsyslog-relp rsyslog-openssl \
               rsyslog-elasticsearch rsyslog-mmnormalize \
               rsyslog-mmfields rsyslog-mmjsonparse \
               rsyslog-kafka

# cleanup image
yum clean all
rm -rf /var/cache/yum
